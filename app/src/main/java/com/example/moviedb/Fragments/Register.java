package com.example.moviedb.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moviedb.Activities.MainWindowActivity;
import com.example.moviedb.Classes.MD5;
import com.example.moviedb.Classes.User;
import com.example.moviedb.R;
import com.example.moviedb.Database.DatabaseHelper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Register extends Fragment {

    EditText username_register,password_register,email_register;
    Button button;
    MD5 encryptor;
    private FirebaseAuth mAuth;
    private User user;

    private DatabaseHelper databaseHelper;

    public Register() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);

        username_register =  v.findViewById(R.id.register_username);
        password_register = v.findViewById(R.id.register_password);
        email_register =  v.findViewById(R.id.register_email);
        button = v.findViewById(R.id.button3);
        encryptor = new MD5();
        mAuth = FirebaseAuth.getInstance();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //postDataToSQLite();
                FireBase();
            }
        });

        return v;
    }

    private void FireBase(){


            String email = email_register.getText().toString();
            String password = password_register.getText().toString();
            if (email.isEmpty()){
                email_register.setError("Please enter email");
                email_register.requestFocus();
            }
            else if (password.isEmpty()){
                password_register.setError("Please enter your password!");
                password_register.requestFocus();
            }
            else if (email.isEmpty() && password.isEmpty()){
                Toast.makeText(getContext(),"Fields are Empty!",Toast.LENGTH_LONG).show();
            }
            else if (!(email.isEmpty() && password.isEmpty())){
                mAuth.createUserWithEmailAndPassword(email,encryptor.MD5(password)).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(getContext(),"Succesfully Registered",Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getContext(),"Error!",Toast.LENGTH_LONG).show();

                        }
                    }
                });
            }
        }

    private void postDataToSQLite(){

        String text_email = email_register.getText().toString().trim();
        String text_password = password_register.getText().toString().trim();
        String text_username = username_register.getText().toString().trim();
        if (text_email.isEmpty()){
            email_register.setError("Please enter your email!");
            email_register.requestFocus();
        }
        else if (text_password.isEmpty()){
            password_register.setError("Please enter your password!");
            password_register.requestFocus();
        }
        else if (text_username.isEmpty()){
            username_register.setError("Please enter your password!");
            username_register.requestFocus();
        }

        if(databaseHelper.checkUser(text_email)){
            user.setName(text_username);
            user.setEmail(text_email);
            user.setPassword(text_password);

            //databaseHelper.addUser(user);
            Toast.makeText(getContext(), "Account created.", Toast.LENGTH_SHORT).show();
            emptyInputEditText();
        }else{
            Toast.makeText(getContext(),"Email is already used!", Toast.LENGTH_SHORT).show();
        }
        }


    private void emptyInputEditText(){
        username_register.setText(null);
        password_register.setText(null);
        email_register.setText(null);
    }

}