package com.example.moviedb.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviedb.Activities.MainWindowActivity;
import com.example.moviedb.Classes.MD5;
import com.example.moviedb.Database.DatabaseHelper;
import com.example.moviedb.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends Fragment implements View.OnClickListener {

    private TextView email,password;
    private Button login_button,register_button;
    private DatabaseHelper databaseHelper;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    MD5 encryptor = new MD5();
    public Login() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        email = v.findViewById(R.id.login_email);
        password = v.findViewById(R.id.login_pass);

        login_button = v.findViewById(R.id.login_button);
        register_button = v.findViewById(R.id.register_button);
        mFirebaseAuth = FirebaseAuth.getInstance();
        initListeners();
        //initDataBase();

        return v;
    }

    private void initListeners(){
        login_button.setOnClickListener(this);
        register_button.setOnClickListener(this);
    }

    private void initDataBase(){
        databaseHelper = new DatabaseHelper(getContext());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                //verifyFromSQLite();
                verifyFromFirebase();
                break;
            case R.id.register_button:
                Fragment fragmentRegister = new Register();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container, fragmentRegister);
                fragmentTransaction.commit();
                break;
        }
    }

    private void verifyFromFirebase(){
        final String current_email = email.getText().toString().trim();
        final String current_password = password.getText().toString().trim();
        if (current_email.isEmpty()){
            email.setError("Please enter your email!");
            email.requestFocus();
        }
        else if (current_password.isEmpty()){
            password.setError("Please enter your password!");
            password.requestFocus();
        }

        else if (current_email.isEmpty() && current_password.isEmpty()){
            Toast.makeText(getContext(),"Fields are Empty!",Toast.LENGTH_LONG).show();

        }
        else if (!(current_email.isEmpty() && current_password.isEmpty())){
            mFirebaseAuth.signInWithEmailAndPassword(current_email,encryptor.MD5(current_password)).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        Intent intent = new Intent(getActivity(), MainWindowActivity.class);
                        intent.putExtra("EMAIL",current_email);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getContext(),"Please Try Again!",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    private void verifyFromSQLite(){
        final String current_email = email.getText().toString().trim();
        final String current_password = password.getText().toString().trim();

        if (current_email.isEmpty()){
            email.setError("Please enter your email!");
            email.requestFocus();
        }
        else if (current_password.isEmpty()){
            password.setError("Please enter your password!");
            password.requestFocus();
        }

        else if (current_email.isEmpty() && current_password.isEmpty()){
            Toast.makeText(getContext(),"Fields are Empty!",Toast.LENGTH_LONG).show();

        }

        if(databaseHelper.checkUser(current_email, current_password)){

            Intent intent = new Intent(getActivity(), MainWindowActivity.class);
            emptyInputEditText();
            startActivity(intent);

        }else
        {
            Toast.makeText(getContext(), "Error at login.", Toast.LENGTH_SHORT).show();
        }

    }

    private void emptyInputEditText(){
        email.setText(null);
        password.setText(null);
    }
}

